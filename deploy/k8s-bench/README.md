### Introduction

Helm Chart for installing k8s-bench

#### Installation

```shell
helm upgrade --install erpnext-13 -n erpnext k8s-bench --set ingress.enabled=true
```

Notes:

- This will make the bench api available on http://k8s-bench.localhost/api-docs by default
- Change the environment variables (`envVars`) and other values in `values.yaml` in case of production installation. Use secure passwords.
