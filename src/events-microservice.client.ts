import { ConfigService } from '@nestjs/config';
import {
  ClientsProviderAsyncOptions,
  MqttOptions,
  Transport,
} from '@nestjs/microservices';
import {
  EVENTS_HOST,
  EVENTS_PORT,
  EVENTS_PASSWORD,
  EVENTS_PROTO,
  EVENTS_USER,
  EVENTS_CLIENT_ID,
} from './constants/config-options';

export const BROADCAST_EVENT = 'BROADCAST_EVENT';

export const eventsConnectionFactory = (config: ConfigService): MqttOptions => {
  const url = `${config.get(EVENTS_PROTO)}://${config.get(
    EVENTS_USER,
  )}:${config.get(EVENTS_PASSWORD)}@${config.get(EVENTS_HOST)}:${config.get(
    EVENTS_PORT,
  )}`;
  const clientId = config.get(EVENTS_CLIENT_ID);
  return {
    transport: Transport.MQTT,
    options: { url, clientId, protocolVersion: 5 },
  };
};

export const eventsClient: ClientsProviderAsyncOptions = {
  useFactory: eventsConnectionFactory,
  name: BROADCAST_EVENT,
  inject: [ConfigService],
};
