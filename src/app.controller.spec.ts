import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SERVICE_NAME } from './constants/app-constants';
import { BENCH_UID } from './constants/config-options';

export const appServiceResponse = {
  service: SERVICE_NAME,
  uuid: BENCH_UID,
};

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        AppService,
        {
          provide: ConfigService,
          useValue: {
            get: env => env,
          },
        },
      ],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "{ service: SERVICE_NAME }"', () => {
      expect(appController.getServiceName()).toEqual(appServiceResponse);
    });
  });
});
