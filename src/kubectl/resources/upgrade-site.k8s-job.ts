import { V1Job } from '@kubernetes/client-node';
import {
  SITES_DIR,
  BASE_SITES_DIR,
  IF_NOT_PRESENT,
  ASSETS_CACHE,
  SET_MAINTENANCE_MODE,
  SITES,
} from '../../constants/app-constants';
import { UpgradeSiteDto } from '../controllers/dtos/upgrade-site-job.dto';

export const UPGRADE_SITE = 'upgrade-site';
export const JQ_IMAGE = 'stedolan/jq';

export const generateUpdateSiteJobJsonTemplate = (
  payload: UpgradeSiteDto,
  namespace: string,
  pyImage: string,
  nginxImage: string,
  pvc: string,
  benchUid: string,
  erpnextVersion: string,
  upgradeSiteConfigMapName: string,
  imagePullSecretName?: string,
): V1Job => {
  return {
    metadata: {
      name: `${UPGRADE_SITE}-${payload.siteName}`,
      namespace,
      annotations: { benchUid },
    },
    spec: {
      backoffLimit: 0,
      template: {
        spec: {
          imagePullSecrets: imagePullSecretName
            ? [{ name: imagePullSecretName }]
            : [],
          securityContext: {
            supplementalGroups: [1000],
          },
          initContainers: [
            {
              name: SET_MAINTENANCE_MODE,
              image: JQ_IMAGE,
              imagePullPolicy: IF_NOT_PRESENT,
              command: ['/bin/bash', '-c'],
              args: [
                `export TEMP_FILE=$(date +"%Y%m%d%H%M%S");
                cp /data/${payload.siteName}/site_config.json /data/\${TEMP_FILE}.json;
                jq -r '.maintenance_mode=1 | .pause_scheduler=1' /data/\${TEMP_FILE}.json > /data/${payload.siteName}/site_config.json;
                chown 1000:1000 /data/${payload.siteName}/site_config.json;
                rm /data/\${TEMP_FILE}.json;`,
              ],
              volumeMounts: [
                {
                  name: BASE_SITES_DIR,
                  mountPath: '/data',
                },
              ],
            },
            {
              name: 'populate-assets',
              image: nginxImage,
              command: ['/bin/bash', '-c'],
              args: ['rsync -a --delete /var/www/html/assets/frappe /assets'],
              volumeMounts: [
                {
                  name: ASSETS_CACHE,
                  mountPath: '/assets',
                },
              ],
            },
            {
              name: 'backup',
              image: payload.basePyImage,
              args: ['backup'],
              env: [
                {
                  name: SITES,
                  value: payload.siteName,
                },
              ],
              imagePullPolicy: IF_NOT_PRESENT,
              volumeMounts: [
                {
                  name: BASE_SITES_DIR,
                  mountPath: '/home/frappe/frappe-bench/sites',
                },
              ],
            },
          ],
          containers: [
            {
              name: UPGRADE_SITE,
              image: `${pyImage}:${erpnextVersion}`,
              command: ['/home/frappe/frappe-bench/env/bin/python'],
              args: ['/home/frappe/frappe-bench/commands/upgrade_site.py'],
              imagePullPolicy: IF_NOT_PRESENT,
              volumeMounts: [
                {
                  name: SITES_DIR,
                  mountPath: '/home/frappe/frappe-bench/sites',
                },
                {
                  name: BASE_SITES_DIR,
                  mountPath: '/opt/base-sites',
                },
                {
                  name: UPGRADE_SITE,
                  mountPath: '/home/frappe/frappe-bench/commands',
                },
                {
                  name: ASSETS_CACHE,
                  mountPath: '/home/frappe/frappe-bench/sites/assets',
                },
              ],
              env: [
                { name: 'SITE_NAME', value: payload.siteName },
                { name: 'FROM_BENCH_PATH', value: '/opt/base-sites' },
              ],
            },
          ],
          restartPolicy: 'Never',
          volumes: [
            {
              name: SITES_DIR,
              persistentVolumeClaim: {
                claimName: pvc,
                readOnly: false,
              },
            },
            {
              name: BASE_SITES_DIR,
              persistentVolumeClaim: {
                claimName: payload.basePvc,
                readOnly: false,
              },
            },
            {
              name: UPGRADE_SITE,
              configMap: {
                name: upgradeSiteConfigMapName,
              },
            },
            {
              name: ASSETS_CACHE,
              emptyDir: {},
            },
          ],
        },
      },
    },
  };
};
