import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import * as k8s from '@kubernetes/client-node';
import { ConfigService } from '@nestjs/config';
import { HttpException } from '@nestjs/common';
import { KubectlService } from '../../aggregates/kubectl/kubectl.service';
import { GetIngressQuery } from './get-ingress.query';
import { ERPNEXT_NAMESPACE } from '../../../constants/config-options';

@QueryHandler(GetIngressQuery)
export class GetIngressHandler implements IQueryHandler<GetIngressQuery> {
  constructor(
    private readonly kubectl: KubectlService,
    private readonly config: ConfigService,
  ) {}

  async execute(query: GetIngressQuery) {
    const netV1APi = this.kubectl.k8sConfig.makeApiClient(k8s.NetworkingV1Api);
    const namespace = this.config.get<string>(ERPNEXT_NAMESPACE);

    try {
      const response = await netV1APi.readNamespacedIngress(
        query.ingressName,
        namespace,
        'true',
      );
      delete response?.body?.metadata?.managedFields;
      return response?.body;
    } catch (error) {
      throw new HttpException(error, error?.statusCode || 500);
    }
  }
}
