import { Watch } from '@kubernetes/client-node';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientMqtt } from '@nestjs/microservices';
import { from } from 'rxjs';
import { serializeError } from 'serialize-error';
import { ERPNEXT_NAMESPACE } from '../../../constants/config-options';
import { BROADCAST_EVENT } from '../../../events-microservice.client';
import { KubectlService } from '../../aggregates/kubectl/kubectl.service';
import { ResourceEventType } from './resource-event.type';

export const JobWatchSuccessEvent = 'JobWatchSuccessEvent';
export const JobWatchErrorEvent = 'JobWatchErrorEvent';

@Injectable()
export class WatchJobsService {
  constructor(
    @Inject(BROADCAST_EVENT)
    private readonly publisher: ClientMqtt,
    private readonly kube: KubectlService,
    private readonly config: ConfigService,
  ) {}

  start() {
    const id = `batch/jobs`;
    const watch = new Watch(this.kube.k8sConfig);
    const namespace = this.config.get(ERPNEXT_NAMESPACE);
    const uri = `/apis/batch/v1/namespaces/${namespace}/jobs`;

    from(
      watch.watch(
        uri,
        {},
        (phase: ResourceEventType, apiObj: any, watchObj: any) => {
          this.emitSuccessEvent(phase, apiObj, watchObj);
        },
        err => {
          this.emitErrorEvent(id, err);
          Logger.error(
            `watch on resource ${id} failed: ${this.errorToJson(err)}`,
          );
          process.exit(1);
        },
      ),
    ).subscribe({
      next: res => {
        Logger.log('Watching Jobs', this.constructor.name);
      },
      error: err => {
        this.emitErrorEvent(id, err);
        Logger.error(
          `watch on resource ${id} failed: ${this.errorToJson(err)}`,
        );
        process.exit(1);
      },
    });
  }

  errorToJson(err: unknown): string {
    err = serializeError(err);
    if (typeof err === 'string') {
      return err;
    }
    return JSON.stringify(err);
  }

  emitSuccessEvent(phase: ResourceEventType, apiObj: any, watchObj: any) {
    this.publisher.emit(JobWatchSuccessEvent, { phase, apiObj, watchObj });
  }

  emitErrorEvent(id: string, error) {
    this.publisher.emit(JobWatchErrorEvent, { id, error });
  }
}
