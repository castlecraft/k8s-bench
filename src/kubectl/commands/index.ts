import { CreateNewSiteJobHandler } from './create-new-site-job/create-new-site-job.handler';
import { CreateSiteIngressHandler } from './create-site-ingress/create-site-ingress.handler';
import { DeleteSiteIngressHandler } from './delete-site-resources/delete-site-resources.handler';
import { DeleteSiteHandler } from './delete-site/delete-site.handler';
import { ExecuteBenchCommandsJobHandler } from './execute-bench-commands/execute-bench-commands-job.handler';
import { PatchIngressHandler } from './patch-ingress/patch-ingress.handler';
import { UpgradeSiteHandler } from './upgrade-site/upgrade-site.handler';

export const KubeCommandHandlers = [
  CreateNewSiteJobHandler,
  CreateSiteIngressHandler,
  DeleteSiteIngressHandler,
  DeleteSiteHandler,
  UpgradeSiteHandler,
  PatchIngressHandler,
  ExecuteBenchCommandsJobHandler,
];
