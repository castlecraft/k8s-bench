import { ICommand } from '@nestjs/cqrs';

export class CreateSiteIngressCommand implements ICommand {
  constructor(
    public readonly svcName: string,
    public readonly jobName: string,
    public readonly namespace: string,
    public readonly wildcardDomain: string,
    public readonly wildcardTlsSecretName: string,
  ) {}
}
