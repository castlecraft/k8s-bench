import { InternalServerErrorException, Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { UpgradeSiteAggregateService } from '../../aggregates/upgrade-site-aggregate/upgrade-site-aggregate.service';
import { PatchIngressCommand } from './patch-ingress.command';

@CommandHandler(PatchIngressCommand)
export class PatchIngressHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: UpgradeSiteAggregateService,
  ) {}

  async execute(command: PatchIngressCommand) {
    const { payload } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    try {
      const res = await aggregate.patchIngress(
        payload.siteName,
        payload.service,
      );
      aggregate.commit();
      return res;
    } catch (error) {
      Logger.error(error, error.toString(), this.constructor.name);
      throw new InternalServerErrorException(error);
    }
  }
}
