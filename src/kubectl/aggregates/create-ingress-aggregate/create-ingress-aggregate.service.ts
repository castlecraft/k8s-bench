import { Injectable, Inject } from '@nestjs/common';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import * as k8s from '@kubernetes/client-node';
import { AggregateRoot } from '@nestjs/cqrs';

import { CREATE_NEW_SITE } from '../../resources/create-site.k8s-job';
import { generateIngressTemplate } from '../../resources/create-ingress.k8s-ingress';
import { NewSiteCreatedEvent } from '../../events/new-site-created/new-site-created.event';
import {
  INGRESS_CONFIG,
  IngressConfig,
} from '../../providers/ingress-config.provider';
import { KubectlService } from '../kubectl/kubectl.service';

@Injectable()
export class CreateIngressAggregateService extends AggregateRoot {
  constructor(
    @Inject(INGRESS_CONFIG)
    private readonly ingressConfig: IngressConfig,
    private readonly kubectl: KubectlService,
  ) {
    super();
  }

  createIngress(
    jobName: string,
    namespace: string,
    serviceName: string,
    wildcardDomain?: string,
    wildcardTlsSecretName?: string,
  ) {
    const siteName = jobName.replace(`${CREATE_NEW_SITE}-`, '');
    const networkingV1Api = this.kubectl.k8sConfig.makeApiClient(
      k8s.NetworkingV1Api,
    );

    if (wildcardDomain) {
      wildcardDomain = `*.${wildcardDomain}`;
    }

    const ingressBody = generateIngressTemplate(
      siteName,
      serviceName,
      this.ingressConfig,
      wildcardDomain,
      wildcardTlsSecretName,
    );
    return from(
      networkingV1Api.createNamespacedIngress(namespace, ingressBody),
    ).pipe(
      map(data => {
        this.apply(new NewSiteCreatedEvent(siteName));
        return data;
      }),
    );
  }
}
