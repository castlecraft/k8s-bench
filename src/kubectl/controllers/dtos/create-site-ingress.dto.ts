import { ApiProperty } from '@nestjs/swagger';
import { IsFQDN, IsString } from 'class-validator';

export class CreateSiteIngressDto {
  @IsString()
  @ApiProperty()
  svcName: string;
  @IsString()
  @ApiProperty()
  jobName: string;
  @IsString()
  @ApiProperty()
  namespace: string;
  @IsFQDN()
  @ApiProperty()
  wildcardDomain: string;
  @IsString()
  @ApiProperty()
  wildcardTlsSecretName: string;
}
