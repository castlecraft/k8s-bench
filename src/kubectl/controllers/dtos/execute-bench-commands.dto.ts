import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class ExecuteBenchCommandsDto {
  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  jobName: string;

  @IsArray()
  @IsNotEmpty()
  @ApiProperty()
  commands: string[];

  @IsArray()
  @IsNotEmpty()
  @ApiProperty()
  options: string[];

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  benchUid: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  areAssetsRequired: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  isNewImage: boolean;
}
