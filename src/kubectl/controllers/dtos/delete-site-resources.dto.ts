import { ApiProperty } from '@nestjs/swagger';
import { IsFQDN } from 'class-validator';

export class DeleteSiteResourcesDto {
  @IsFQDN()
  @ApiProperty()
  siteName: string;
}
