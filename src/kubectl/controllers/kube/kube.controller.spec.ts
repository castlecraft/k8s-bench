import { Test, TestingModule } from '@nestjs/testing';
import { CqrsModule } from '@nestjs/cqrs';
import { KubeController } from './kube.controller';
import { BenchGuard } from '../../../auth/guards/bench/bench.guard';
import { ConfigService } from '@nestjs/config';

describe('Kube Controller', () => {
  let controller: KubeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [KubeController],
      providers: [{ provide: ConfigService, useValue: {} }],
    })
      .overrideGuard(BenchGuard)
      .useValue({})
      .compile();

    controller = module.get<KubeController>(KubeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
