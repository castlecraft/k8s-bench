import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { BENCH_SECRET, BENCH_UID } from '../../../constants/config-options';
import { AUTHORIZATION } from '../../../constants/app-constants';

@Injectable()
export class BenchGuard implements CanActivate {
  constructor(private readonly config: ConfigService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const httpContext = context.switchToHttp();
    const request = httpContext.getRequest();
    const authHeader = request.headers[AUTHORIZATION.toLowerCase()];
    if (authHeader && this.verifyAuthorization(authHeader)) {
      return true;
    }
    return false;
  }

  verifyAuthorization(authorizationHeader: string) {
    try {
      const basicAuthHeader = authorizationHeader.split(' ')[1];
      const [benchUid, benchSecret] = Buffer.from(basicAuthHeader, 'base64')
        .toString()
        .split(':');

      const configBenchUid = this.config.get<string>(BENCH_UID);
      const configBenchSecret = this.config.get<string>(BENCH_SECRET);

      if (configBenchUid === benchUid && configBenchSecret === benchSecret) {
        return true;
      }
    } catch (error) {
      throw new ForbiddenException();
    }
    return false;
  }
}
